import React, {Component} from 'react';
import './App.css';
import PrimeDecomposition from "./Prime/PrimeDecomposition";
import PrimeList from "./Prime/PrimeList";
import Latex from 'react-latex';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: null
    };
    this.primeDecomposition = new PrimeDecomposition(this.props);
    this.primeList = new PrimeList(this.props);
  }

  numberHandleChange = (e) => {
    this.setState({number: +e.target.value});
  };

  buttonDecomposition = (e) => {
    this.primeDecomposition.setNumber(this.state.number);
    this.setState({});
  };

  buttonList = (e) => {
    this.primeList.setNumber(this.state.number);
    this.setState({});
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div>
            <label>Entrez un nombre : </label>
            <input onChange={this.numberHandleChange}/>
            <div>
              <button onClick={this.buttonDecomposition}>Décomposition en facteur de nombres premiers</button>
              {this.primeDecomposition.render()}
            </div>
            <div>
              <button onClick={this.buttonList}>Liste des nombres premiers inférieurs</button>
              {this.primeList.render()}
            </div>
          </div>
        </header>
      </div>
    );
  }
}

export default App;
