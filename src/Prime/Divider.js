
export default class Divider {
    constructor(prime, exponent) {
        this.prime = prime;
        this.exponent = exponent;
    };

    getLatexString() {
        if (1 === this.exponent) {
            return this.prime;
        }
        return this.prime + '^' + this.exponent;
    }
}