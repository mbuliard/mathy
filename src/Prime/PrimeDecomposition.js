import React, {Component} from "react";
import Latex from "react-latex";
import Divider from "./Divider";

class PrimeDecomposition extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: null,
            decomposition: []
        };
    }

    setNumber(number) {
        if (number && Number.isInteger(number)) {
            this.state.number = number;
            this._decomposition(number);
        } else {
            this.state.number = null;
        }
    }

    render() {
        if (this.state.number) {
            if (_isPrime(this.state.number, this.state.decomposition[0])) {
                return (
                    <span>
                        {this.state.number} est un nombre premier
                    </span>
                )
            }
            return (
                <span>
                    <Latex>{this._getLatexString()}</Latex>
                </span>
            );
        }
        return (<span />);
    }

    _decomposition(number) {
        this.state.decomposition = [];
        let remaining = this._handleNegative(number);
        remaining = this._recursiveDecomposition(remaining, 2, Math.sqrt(remaining));
        this._addPrimalOrOne(remaining, number);
    }

    _addPrimalOrOne(remaining, number) {
        if (1 === number || 1 < remaining) {
            this._addPrimeFactor(remaining, 1);
        }
    }

    _handleNegative(number) {
        if (number < 0) {
            this._addPrimeFactor(-1, 1);
            return - number;
        }
        return number;
    }

    _recursiveDecomposition(remaining, divider, squareLimit) {
        remaining = this._addPrimeFactorIfDivides(divider, remaining);
        if (divider <= squareLimit) {
            return this._recursiveDecomposition(remaining, divider + 1, squareLimit);
        }
        return remaining;
    }

    _addPrimeFactorIfDivides(prime, number) {
        let exponent = _howManyTimeItDivides(number, prime, 0);
        if (exponent) {
            this._addPrimeFactor(prime, exponent);
            number = number / (prime ** exponent);
        }

        return number;
    }

    _addPrimeFactor(prime, exponent) {
        this.state.decomposition.push(new Divider(prime, exponent));
    }

    _getLatexString() {
        return '$' + this.state.number + ' = ' +
            this.state.decomposition.map(divider => divider.getLatexString()).join(" \\times ") + '$';
    }
}

const _howManyTimeItDivides = (number, divider, exponent) => {
    if (0 === number % (divider ** (exponent + 1))) {
        return _howManyTimeItDivides(number, divider, exponent + 1);
    }

    return exponent;
};

const _isPrime = (number, firstFactor) => {
    return (
        1 !== number
        && -1 !== number
        && number === firstFactor.prime
    );
};

export default PrimeDecomposition;