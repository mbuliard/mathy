import React, {Component} from "react";

class PrimeList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: null,
            primeList: []
        };
    }


    setNumber(number) {
        if (number && Number.isInteger(number)) {
            this.state.number = number;
            this.setList(number);
        } else {
            this.state.number = null;
        }
    }

    setList(number) {
        for(let i = 2; i <= number; i++) {
            this.addToListIfPrime(i);
        }
    }

    addToListIfPrime(number) {
        if (this.isMultiPleFromList(number)) {
            this.state.primeList.push(number);
        }
    }

    isMultiPleFromList(number) {
        for(const prime of this.state.primeList) {
            if (number % prime === 0) {
                return false;
            }
        }
        return true;
    }

    render() {
        return (
            <span>{this.state.primeList.join(', ')}</span>
        );
    }
}

export default PrimeList;